from main import ItemList
import pytest


@pytest.fixture
def items():
    return ItemList()


def test_empty_list(items):
    assert items.show() is None


def test_add_single_item(items):
    items.add('a')
    assert items.show() == 'a null'


def test_add_two_items(items):
    items.add('hello')
    items.add('world')
    assert items.show() == 'hello world null'


def test_add_many_items(items):
    items.add('hello')
    items.add('world')
    items.add('!')
    assert items.show() == 'hello world ! null'


def test_add_integers_or_floats(items):
    items.add('a')
    items.add(101)
    items.add('b')
    items.add(1001.99)
    assert items.show() == 'a 101 b 1001.99 null'


def test_convert_to_integers(items):
    items.add('a')
    items.add(101)
    items.add(-1)
    # could convert float->int, but instructions indicate we should assume values are strings
    items.add(1001.99)
    items.convert_to_int()
    assert items.show() == '0 101 -1 0 null'


def test_reverse(items):
    items.add('a')
    items.add('b')
    items.add('c')
    items.add('d')
    items.add('e')
    items.add('f')
    items.reverse()
    assert items.show() == 'f e d c b a null'


def test_long_string(items):
    long_string = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras tristique volutpat imperdiet. '
    long_string += 'Etiam volutpat ante sapien, at tincidunt purus tempus vitae. Quisque in sapien in nunc aliquet mollis '
    long_string += 'in vitae sem. Nunc gravida purus ut porta dapibus. Phasellus eu dolor scelerisque eros dapibus '
    long_string += 'sollicitudin et nec justo. Proin tincidunt pharetra auctor. Suspendisse pellentesque neque id accumsan '
    long_string += 'venenatis. Vestibulum feugiat nunc ac felis convallis gravida. Cras porta et urna semper gravida. '
    long_string += 'Proin finibus felis eu nisl dignissim eleifend. Donec in ultricies nisl, id mattis odio. Proin id mauris '
    long_string += 'tortor. Curabitur hendrerit nisi dui, nec dapibus justo venenatis vitae.'
    items.add('a')
    items.add(long_string)
    assert items.show() == f'a {long_string} null'


def test_weird_chars(items):
    items.add('a')
    items.add('!@£$%^&*(){}:"|<>?~±')
    assert items.show() == 'a !@£$%^&*(){}:"|<>?~± null'


def test_null_added(items):
    items.add('a')
    items.add()
    items.add('')
    assert items.show() == 'a null'


def test_reverse_empty(items):
    items.reverse()
    assert items.show() is None
