# coding-challenge-18

JLP Coding Challenge 18

## Reflections

- as a non-Computer Science person, the concept of a LinkedList was brand new to me - getting the initial concept down was probably the hardest bit (... or maybe wrapping my head around how to do `reverse()` ...). TDD helped here, although exactly _how_ it helped is probably hidden within commit `1846b43`
- I feel like there are easier ways of doing this sort of thing in Python - this might reflect on why it is not a built-in
- I suspect recency bias is a factor in my code being slightly clunky - I'd just been reading about use of Python classes, and felt like this overcomplicated things for me
- recursion was really powerful here, reflected in a bit of refactoring. I could probably have used this more

---

## Usage

### Local Python

If you have `python3.7` with `pipenv` installed, then you can:

- `pipenv install --dev` to initialise the virtual environment for the first time, then `pipenv shell`:
  - `python3 main.py` to execute the specified scenarios,
  - `flake8` to run linting,
  - `pytest -s -v` to run all tests, inc. coverage,
  - `ptw` to run tests continuously while developing

### Docker

To just see the code run (with tests), then with `docker` installed and running:

```sh
docker build -t challenge-18 .
docker run challenge-18
```

---

## Challenge Instructions

Linked lists appear on the curriculum of every compter science course (well they did 30 years ago!). A simple linked list is a structure where each node in the list contains a value and an optional reference to the next node. In Kotlin a node for a linked list containing strings could be defined like this:

```java
class Node (
    val item:String
    val next:Node? = null // if this is set to null there are no more items in the list.
)

val aNode = Node("world")
val anotherNode = Node("hello",aNode)

//An alternative way of creating a list
val firstNode = Node("hello", Node("world"))
```

This challenge is to create a few functions to work with a linked list:

- [x] Create a function that takes the first node of a linked list of strings and returns a string that can be printed out to show all of the members of the list. Using the sample code above `getDescription(firstNode)` should return `"hello world null"`.
- [x] Create a function to add a value onto the end of a list. So `addToList(firstNode, "!")` and then running `getDescription(firstNode)` should result in `"hello world ! null"`.
- [x] Create a function that takes a linked list of strings and returns a linked list of integers. If the string cannot be converted to an integer, convert it to 0.
- [x] Create a function that takes a linked list of strings and reverses it. So a list with a description of `"a b c d e f null"` would be converted to a list with a description of `"f e d c b a null"`.

> A top tip - avoid creating functions to convert a linked list to/from an array to make creating the other functions easier.
