class Item:
    def __init__(self, value=None, next=None):
        self.value = str(value)
        self.next = next


class ItemList:
    def __init__(self):
        self.pointer = None

    def show(self):
        i = self.pointer
        if i:
            return self._show_helper(self.pointer)

    def _show_helper(self, i):
        if i.next is None:
            # 'null' required to be printed when final Item in List
            return f'{i.value} null'
        else:
            return f'{i.value} {self._show_helper(i.next)}'

    def add(self, value=''):
        if value == '':
            return
        if not self.pointer:
            self.pointer = Item(value)
            return
        i = self.pointer
        while i.next:
            i = i.next
        i.next = Item(value=value)

    def convert_to_int(self):
        i = self.pointer
        while i:
            try:
                i.value = int(i.value)
            except ValueError:
                i.value = 0
            i = i.next

    def reverse(self):
        if self.pointer is not None:
            self._reverse_helper(self.pointer, None)

    def _reverse_helper(self, i, prev):
        # if next item is empty, it is last item and we need to move the pointer and update it
        if i.next is None:
            # set the pointer to this item
            self.pointer = i
            # set the next item to be the preceding item
            i.next = prev
            return

        # ... otherwise we need to save the next item for when we call this again
        next = i.next
        # ... and move to the next item
        i.next = prev
        # recursion!
        self._reverse_helper(next, i)


print('')
print('------ ItemList: add() + show() ------')
items = ItemList()
items.add('hello')
items.add('world')
items.add('!')
print(items.show())
print('--------------------------------------')

print('----- ItemList: convert_to_int() -----')
items = ItemList()
items.add('a')
items.add(101)
items.add(-1)
items.add(1001.99)
items.add('z')
print(items.show())
items.convert_to_int()
print(items.show())
print('--------------------------------------')

print('-------- ItemList: reverse() ---------')
items = ItemList()
items.add('a')
items.add('b')
items.add('c')
items.add('d')
items.add('e')
items.add('f')
print(items.show())
items.reverse()
print(items.show())
print('--------------------------------------')
print('')
